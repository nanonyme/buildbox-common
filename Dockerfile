FROM debian:bullseye

WORKDIR /app

RUN apt-get update && apt-get install -y \
    clang-format \
    cmake \
    g++ \
    git \
    libabsl-dev \
    libbenchmark-dev \
    libc-ares-dev \
    libssl-dev \
    pkg-config \
    sudo \
    uuid-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ARG EXTRA_CMAKE_FLAGS=
ARG BUILD_TESTS=OFF

# The grpc pkg-config file in Debian 11 is missing the Abseil libraries,
# which breaks static linking. The upstream pkg-config file in recent grpc
# seems correct, though. Patch pkg-config file in this container:
RUN apt-get update && apt-get install -y grpc++ && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && sed -i -e '/^Libs.private:/s/$/ -labsl_hash -labsl_city -labsl_raw_hash_set -labsl_hashtablez_sampler -labsl_exponential_biased -labsl_statusor -labsl_status -labsl_cord -labsl_bad_optional_access -labsl_bad_variant_access -labsl_str_format_internal -labsl_synchronization -labsl_graphcycles_internal -labsl_stacktrace -labsl_symbolize -labsl_debugging_internal -labsl_demangle_internal -labsl_malloc_internal -labsl_time -labsl_civil_time -labsl_strings -labsl_strings_internal -labsl_base -labsl_spinlock_wait -labsl_int128 -labsl_throw_delegate -labsl_raw_logging_internal -labsl_log_severity -labsl_time_zone/' /usr/lib/$(uname -m)-linux-gnu/pkgconfig/grpc.pc


# Add non-root user for devcontainer use
RUN groupadd -r builder && useradd --shell /bin/bash -m -r -g builder builder
# Grant access to apt for non-root user to install packages
RUN echo "builder ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

COPY . /buildbox-common

RUN cd /buildbox-common && mkdir build && cd build && cmake -DBUILD_TESTING=${BUILD_TESTS} "${EXTRA_CMAKE_FLAGS}" .. && make -j$(nproc) install
