/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_connectionoptions_commandline.h>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>

namespace buildboxcommon {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

ConnectionOptionsCommandLine::ConnectionOptionsCommandLine(
    const std::string &serviceName, const std::string &commandLinePrefix,
    const bool connectionRequired)
{
    d_spec.emplace_back(commandLinePrefix + "remote",
                        "URL for the " + serviceName + " service",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        (connectionRequired ? ArgumentSpec::O_REQUIRED
                                            : ArgumentSpec::O_OPTIONAL),
                        ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(commandLinePrefix + "instance",
                        "Name of the " + serviceName + " instance",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue(""));
    d_spec.emplace_back(commandLinePrefix + "server-cert",
                        "Public server certificate for " + serviceName +
                            " TLS (PEM-encoded)",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(commandLinePrefix + "client-key",
                        "Private client key for " + serviceName +
                            " TLS (PEM-encoded)",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(commandLinePrefix + "client-cert",
                        "Private client certificate for " + serviceName +
                            " TLS (PEM-encoded)",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(
        commandLinePrefix + "access-token",
        "Access Token for authentication " + serviceName +
            " (e.g. JWT, OAuth access token, etc), "
            "will be included as an HTTP Authorization bearer token",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(commandLinePrefix + "token-reload-interval",
                        "How long to wait before refreshing access token",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(commandLinePrefix + "googleapi-auth",
                        "Use GoogleAPIAuth for " + serviceName + " service",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue(false));
    d_spec.emplace_back(commandLinePrefix + "retry-limit",
                        "Number of times to retry on grpc errors for " +
                            serviceName + " service",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("4"));
    d_spec.emplace_back(commandLinePrefix + "retry-delay",
                        "How long to wait in milliseconds before the first "
                        "grpc retry for " +
                            serviceName + " service",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("1000"));
    d_spec.emplace_back(commandLinePrefix + "request-timeout",
                        "Sets the timeout for gRPC requests in seconds. "
                        "Set to 0 for no timeout.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("0"));
    d_spec.emplace_back(commandLinePrefix + "min-throughput",
                        "Sets the minimum throughput for gRPC requests in "
                        "bytes per seconds.\n"
                        "The value may be suffixed with K, M, G or T.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("0"));
    d_spec.emplace_back(commandLinePrefix + "keepalive-time",
                        "Sets the period for gRPC keepalive pings in seconds. "
                        "Set to 0 to disable keepalive pings.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("0"));
    d_spec.emplace_back(commandLinePrefix + "load-balancing-policy",
                        "Which grpc load balancing policy to use for " +
                            serviceName +
                            " service.\n"
                            "Valid options are 'round_robin' and 'grpclb'",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
}

bool ConnectionOptionsCommandLine::configureChannel(
    const CommandLine &cml, const std::string &commandLinePrefix,
    ConnectionOptions *channel)
{
    if (channel == nullptr) {
        BUILDBOX_LOG_ERROR("invalid argument: 'channel' is nullptr");
        return false;
    }

    resetChannelOptions(channel);
    updateChannelOptions(cml, commandLinePrefix, channel);

    return true;
}

bool ConnectionOptionsCommandLine::updateChannelOptions(
    const CommandLine &cml, const std::string &commandLinePrefix,
    ConnectionOptions *channel)
{
    if (channel == nullptr) {
        BUILDBOX_LOG_ERROR("invalid argument: 'channel' is nullptr");
        return false;
    }

    std::string optionName = commandLinePrefix + "remote";
    if (cml.exists(optionName))
        channel->d_url = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "instance";
    if (cml.exists(optionName))
        channel->d_instanceName = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "server-cert";
    if (cml.exists(optionName))
        channel->d_serverCertPath = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "client-key";
    if (cml.exists(optionName))
        channel->d_clientKeyPath = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "client-cert";
    if (cml.exists(optionName))
        channel->d_clientCertPath = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "access-token";
    if (cml.exists(optionName))
        channel->d_accessTokenPath = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "token-reload-interval";
    if (cml.exists(optionName))
        channel->d_tokenReloadInterval = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "googleapi-auth";
    if (cml.exists(optionName))
        channel->d_useGoogleApiAuth = cml.getBool(optionName);

    optionName = commandLinePrefix + "retry-limit";
    if (cml.exists(optionName))
        channel->d_retryLimit = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "retry-delay";
    if (cml.exists(optionName))
        channel->d_retryDelay = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "request-timeout";
    if (cml.exists(optionName))
        channel->d_requestTimeout = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "min-throughput";
    if (cml.exists(optionName))
        channel->setMinThroughput(cml.getString(optionName));

    optionName = commandLinePrefix + "keepalive-time";
    if (cml.exists(optionName))
        channel->d_keepaliveTime = cml.getString(optionName).c_str();

    optionName = commandLinePrefix + "load-balancing-policy";
    if (cml.exists(optionName))
        channel->d_loadBalancingPolicy = cml.getString(optionName).c_str();

    return true;
}

bool ConnectionOptionsCommandLine::resetChannelOptions(
    ConnectionOptions *channel)
{
    channel->d_url = "";
    channel->d_instanceName = "";
    channel->d_serverCertPath = "";
    channel->d_clientKeyPath = "";
    channel->d_clientCertPath = "";
    channel->d_accessTokenPath = "";
    channel->d_tokenReloadInterval = "";
    channel->d_useGoogleApiAuth = false;
    channel->d_retryLimit = "";
    channel->d_retryDelay = "";
    channel->d_requestTimeout = "";
    channel->d_minThroughput = 0;
    channel->d_keepaliveTime = "";
    channel->d_loadBalancingPolicy = "";

    return true;
}
} // namespace buildboxcommon
