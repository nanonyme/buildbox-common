// Copyright 2018-2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INCLUDED_BUILDBOXCOMMON_REMOTEEXECUTIONCLIENT
#define INCLUDED_BUILDBOXCOMMON_REMOTEEXECUTIONCLIENT

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcclient.h>
#include <buildboxcommon_protos.h>

#include <atomic>
#include <map>
#include <set>

namespace buildboxcommon {

typedef std::shared_ptr<grpc::ClientAsyncReaderInterface<Operation>>
    ReaderPointer;
typedef grpc::ServerWriterInterface<Operation> *WriterPointer;

typedef std::shared_ptr<google::longrunning::Operation> OperationPointer;

class RemoteExecutionClient {
  private:
    std::shared_ptr<buildboxcommon::GrpcClient> d_executionGrpcClient;
    std::shared_ptr<buildboxcommon::GrpcClient> d_actionCacheGrpcClient;

    std::shared_ptr<Execution::StubInterface> d_executionStub;
    std::shared_ptr<Operations::StubInterface> d_operationsStub;
    std::shared_ptr<ActionCache::StubInterface> d_actionCacheStub;

    bool readOperation(grpc::CompletionQueue *cq, ReaderPointer &reader,
                       OperationPointer &operation_ptr, WriterPointer writer,
                       std::function<bool()> stop_requested,
                       const gpr_timespec &firstRequestDeadline,
                       grpc::Status *errorStatus, bool wait = true,
                       bool cancel_on_stop = true,
                       const std::string &operation_name_prefix = "");

    OperationPointer submitExecution(const Digest &actionDigest,
                                     const std::atomic_bool &stop_requested,
                                     bool skipCache,
                                     const ExecutionPolicy *executionPolicy,
                                     bool wait);

    grpc::Status readOperationStream(
        ReaderPointer &reader_ptr, WriterPointer writer_ptr,
        OperationPointer &operation_ptr, grpc::CompletionQueue *cq,
        gpr_timespec requestDeadline, std::function<bool()> stop_requested,
        bool wait, bool cancel_on_stop,
        const std::string &operation_name_prefix = "");

    /**
     * Send an Execute request to the remote server, returning a pointer to
     * the last Operation message received in response.
     *
     * If `wait` is true, then this will be the final Operation response
     * assuming the request was completed, otherwise it will be the first
     * Operation received from the server.
     */
    OperationPointer
    performExecuteRequest(const ExecuteRequest &request,
                          std::function<bool()> stop_requested, bool wait,
                          bool cancel_on_stop, grpc::Status *status);

    /**
     * Send an Execute request to the remote server, returning a pointer to
     * the last Operation message received in response.
     *
     * If `wait` is true, then this will be the final Operation response
     * assuming the request was completed, otherwise it will be the first
     * Operation received from the server.
     *
     * All Operation messages received using this method will be written
     * to the `grpc::ServerWriterInterface` pointed to by `writer_ptr`.
     */
    OperationPointer performExecuteRequest(
        const ExecuteRequest &request, std::function<bool()> stop_requested,
        WriterPointer writer_ptr, bool wait, bool cancel_on_stop,
        grpc::Status *status, const std::string &operation_name_prefix = "");

    /**
     * Send a WaitExecution request to the remote server, returning a pointer
     * to the last Operation message received in response.
     *
     * If `wait` is true, then this will be the final Operation response
     * assuming the request was completed, otherwise it will be the first
     * Operation received from the server (making this method functionally
     * equivalent to `getOperation`).
     *
     * All Operation messages received using this method will be written
     * to the `grpc::ServerWriterInterface` pointed to by `writer_ptr`.
     */
    OperationPointer
    performWaitExecutionRequest(const WaitExecutionRequest &request,
                                std::function<bool()> stop_requested,
                                WriterPointer writer_ptr, bool wait,
                                bool cancel_on_stop, grpc::Status *status,
                                const std::string &operation_name_prefix = "");

  public:
    explicit RemoteExecutionClient(
        std::shared_ptr<buildboxcommon::GrpcClient> executionGrpcClient,
        std::shared_ptr<buildboxcommon::GrpcClient> actionCacheGrpcClient)
        : d_executionGrpcClient(executionGrpcClient),
          d_actionCacheGrpcClient(actionCacheGrpcClient)
    {
    }

    virtual ~RemoteExecutionClient(){};

    virtual void
    init(std::shared_ptr<Execution::StubInterface> executionStub,
         std::shared_ptr<ActionCache::StubInterface> actionCacheStub,
         std::shared_ptr<Operations::StubInterface> operationsStub);

    virtual void init();

    /**
     * Attempts to fetch the ActionResult with the given digest from the action
     * cache and store it in the `result` parameter. The return value
     * indicates whether the ActionResult was found in the action cache.
     * If it wasn't, `result` is not modified.
     *
     */
    virtual bool fetchFromActionCache(const Digest &actionDigest,
                                      const std::set<std::string> &outputs,
                                      ActionResult *result);

    /**
     * Upload a new execution result. The Action message must be uploaded to
     * CAS before calling this method.
     */
    virtual void updateActionCache(const Digest &actionDigest,
                                   const ActionResult &result);

    /**
     * Run the action with the given digest on the given server, waiting
     * synchronously for it to complete. The Action must already be present in
     * the server's CAS.
     */
    virtual ActionResult
    executeAction(const Digest &actionDigest,
                  const std::atomic_bool &stop_requested,
                  bool skipCache = false,
                  const ExecutionPolicy *executionPolicy = nullptr);

    /**
     * Run the action with the given digest on the given server asynchronously.
     * Returns the operation id. The Action must already be present in
     * the server's CAS.
     */
    virtual google::longrunning::Operation
    asyncExecuteAction(const Digest &actionDigest,
                       const std::atomic_bool &stop_requested,
                       bool skipCache = false,
                       const ExecutionPolicy *executionPolicy = nullptr);

    /**
     * Download all output directories and files and store them in the
     * specified directory.
     *
     * Will throw an exception if one or multiple blobs cannot be downloaded.
     */
    virtual void downloadOutputs(buildboxcommon::CASClient *casClient,
                                 const ActionResult &actionResult, int dirfd);

    /*
     * Gets the current Operation object by operation id. Used to check its
     * execution status.
     */
    virtual google::longrunning::Operation
    getOperation(const std::string &operationName);

    /**
     * Sends the CancelOperation RPC
     */
    virtual void cancelOperation(const std::string &operationName);

    /**
     * Gets a list of Operations matching the given filter and page size.
     */
    virtual google::longrunning::ListOperationsResponse
    listOperations(const std::string &name, const std::string &filter,
                   int page_size, const std::string &page_token);

    /**
     * Send an Execute request to the remote server.
     *
     * All Operation messages received using this method will be written
     * to the `grpc::ServerWriterInterface` pointed to by `writer_ptr`.
     */
    grpc::Status
    proxyExecuteRequest(const ExecuteRequest &request,
                        std::function<bool()> stop_requested,
                        WriterPointer writer_ptr,
                        const std::string &operation_name_prefix = "");

    /**
     * Send a WaitExecution request to the remote server.
     *
     * All Operation messages received using this method will be written
     * to the `grpc::ServerWriterInterface` pointed to by `writer_ptr`.
     */
    grpc::Status
    proxyWaitExecutionRequest(const WaitExecutionRequest &request,
                              std::function<bool()> stop_requested,
                              WriterPointer writer_ptr,
                              const std::string &operation_name_prefix = "");

    /**
     * Send a GetOperation request to the remote server.
     *
     * The Operation received in response to this request populates the
     * `response` parameter, and the gRPC status code received from the
     * server is returned.
     */
    grpc::Status proxyGetOperationRequest(const GetOperationRequest &request,
                                          Operation *response);

    /**
     * Send a ListOperations request to the remote server.
     *
     * The response from the server is stored in the `response` parameter,
     * and the gRPC status code received from the server is returned.
     */
    grpc::Status
    proxyListOperationsRequest(const ListOperationsRequest &request,
                               ListOperationsResponse *response);

    /**
     * Send a CancelOperation request to the remote server.
     *
     * The (empty) response from the server is stored in the `response`
     * parameter and the gRPC status code received from the server is
     * returned.
     */
    grpc::Status
    proxyCancelOperationRequest(const CancelOperationRequest &request,
                                google::protobuf::Empty *response);
};
} // namespace buildboxcommon
#endif
