#include <build/bazel/remote/asset/v1/remote_asset_mock.grpc.pb.h>

#include <gtest/gtest.h>

#include <buildboxcommon_assetclient.h>
#include <buildboxcommon_grpcclient.h>

#include <memory>

using namespace buildboxcommon;
using namespace testing;

class ClientTestFixture : public AssetClient, public Test {
  protected:
    std::shared_ptr<MockFetchStub> fetchClient =
        std::make_shared<MockFetchStub>();
    std::shared_ptr<MockPushStub> pushClient =
        std::make_shared<MockPushStub>();

    ClientTestFixture() : AssetClient(std::make_shared<GrpcClient>())
    {
        this->init(fetchClient, pushClient);
        this->d_grpcClient->setInstanceName(clientInstanceName);
        this->d_grpcClient->setRetryLimit(1);
    }

    /********************** MOCK TEST DATA ********************************/
  public:
    static const std::vector<std::string> uris;
    static const std::string selectedUri;
    static const std::vector<std::pair<std::string, std::string>> qualifiers;
    static const std::vector<Qualifier> returnedQualifiers;
    static const std::string clientInstanceName;
    static const Digest digest;
    static const std::chrono::time_point<std::chrono::steady_clock> time;
    static const google::protobuf::Timestamp protobuf_time;
};

const std::vector<std::string> ClientTestFixture::uris = {"www.bloomberg.com",
                                                          "www.google.com"};

const std::string ClientTestFixture::selectedUri = "www.google.com";

const std::vector<std::pair<std::string, std::string>>
    ClientTestFixture::qualifiers = {{"key1", "value1"}, {"key2", "value2"}};

const std::vector<Qualifier> ClientTestFixture::returnedQualifiers = [] {
    Qualifier q1;
    q1.set_name("key1");
    q1.set_value("value1");

    Qualifier q2;
    q2.set_name("key2");
    q2.set_value("value2");
    return std::vector<Qualifier>({std::move(q1), std::move(q2)});
}();

const std::string ClientTestFixture::clientInstanceName = "CasTestInstance123";

const Digest ClientTestFixture::digest = [] {
    Digest d;
    d.set_hash("123");
    d.set_size_bytes(100);
    return d;
}();

const std::chrono::time_point<std::chrono::steady_clock>
    ClientTestFixture::time = std::chrono::steady_clock::now();

const google::protobuf::Timestamp ClientTestFixture::protobuf_time = [] {
    google::protobuf::Timestamp ts;

    const auto seconds =
        std::chrono::time_point_cast<std::chrono::seconds>(time);

    ts.set_seconds(seconds.time_since_epoch().count());
    ts.set_nanos(
        (std::chrono::time_point_cast<std::chrono::nanoseconds>(time) -
         std::chrono::time_point_cast<std::chrono::nanoseconds>(seconds))
            .count());
    return ts;
}();

MATCHER(FetchRequestMatcher, "")
{
    if (std::vector<std::string>(arg.uris().begin(), arg.uris().end()) !=
        ClientTestFixture::uris) {
        return false;
    }
    else if (arg.instance_name() != ClientTestFixture::clientInstanceName) {
        return false;
    }
    else {
        for (std::size_t i = 0; i < ClientTestFixture::qualifiers.size();
             i++) {
            const Qualifier &qualifier = arg.qualifiers()[i];
            if (qualifier.name() != ClientTestFixture::qualifiers[i].first) {
                return false;
            }
            else if (qualifier.value() !=
                     ClientTestFixture::qualifiers[i].second) {
                return false;
            }
        }
    }

    return true;
}

TEST_F(ClientTestFixture, FetchBlobTest)
{
    EXPECT_CALL(*fetchClient, FetchBlob(_, FetchRequestMatcher(), _))
        .WillOnce([](::grpc::ClientContext *context,
                     const FetchBlobRequest &request,
                     FetchBlobResponse *response) {
            response->set_uri(ClientTestFixture::selectedUri);
            for (const Qualifier &qualifier :
                 ClientTestFixture::returnedQualifiers) {
                *response->add_qualifiers() = qualifier;
            }

            *response->mutable_blob_digest() = ClientTestFixture::digest;
            *response->mutable_expires_at() = ClientTestFixture::protobuf_time;
            return grpc::Status::OK;
        });

    const FetchResult res = fetchBlob(uris, qualifiers);

    ASSERT_EQ(res.uri, ClientTestFixture::selectedUri);
    ASSERT_EQ(res.qualifiers, ClientTestFixture::qualifiers);
    ASSERT_EQ(res.expiresAt, ClientTestFixture::time);
    ASSERT_EQ(res.digest, ClientTestFixture::digest);
}

TEST_F(ClientTestFixture, FetchDirectoryTest)
{
    EXPECT_CALL(*fetchClient, FetchDirectory(_, FetchRequestMatcher(), _))
        .WillOnce([](::grpc::ClientContext *context,
                     const FetchDirectoryRequest &request,
                     FetchDirectoryResponse *response) {
            response->set_uri(ClientTestFixture::selectedUri);
            for (const Qualifier &qualifier :
                 ClientTestFixture::returnedQualifiers) {
                *response->add_qualifiers() = qualifier;
            }

            *response->mutable_root_directory_digest() =
                ClientTestFixture::digest;
            *response->mutable_expires_at() = ClientTestFixture::protobuf_time;
            return grpc::Status::OK;
        });

    const FetchResult res = fetchDirectory(uris, qualifiers);

    ASSERT_EQ(res.uri, ClientTestFixture::selectedUri);
    ASSERT_EQ(res.qualifiers, ClientTestFixture::qualifiers);
    ASSERT_EQ(res.expiresAt, ClientTestFixture::time);
    ASSERT_EQ(res.digest, ClientTestFixture::digest);
}

MATCHER(PushBlobRequestMatcher, "")
{
    if (std::vector<std::string>(arg.uris().begin(), arg.uris().end()) !=
        ClientTestFixture::uris) {
        return false;
    }
    else if (arg.instance_name() != ClientTestFixture::clientInstanceName) {
        return false;
    }
    else if (arg.blob_digest() != ClientTestFixture::digest) {
        return false;
    }
    else {
        for (std::size_t i = 0; i < ClientTestFixture::qualifiers.size();
             i++) {
            const Qualifier &qualifier = arg.qualifiers()[i];
            if (qualifier.name() != ClientTestFixture::qualifiers[i].first) {
                return false;
            }
            else if (qualifier.value() !=
                     ClientTestFixture::qualifiers[i].second) {
                return false;
            }
        }
    }

    return true;
}

MATCHER(PushDirectoryRequestMatcher, "")
{
    if (std::vector<std::string>(arg.uris().begin(), arg.uris().end()) !=
        ClientTestFixture::uris) {
        return false;
    }
    else if (arg.instance_name() != ClientTestFixture::clientInstanceName) {
        return false;
    }
    else if (arg.root_directory_digest() != ClientTestFixture::digest) {
        return false;
    }
    else {
        for (std::size_t i = 0; i < ClientTestFixture::qualifiers.size();
             i++) {
            const Qualifier &qualifier = arg.qualifiers()[i];
            if (qualifier.name() != ClientTestFixture::qualifiers[i].first) {
                return false;
            }
            else if (qualifier.value() !=
                     ClientTestFixture::qualifiers[i].second) {
                return false;
            }
        }
    }

    return true;
}

TEST_F(ClientTestFixture, PushBlobTest)
{
    EXPECT_CALL(*pushClient, PushBlob(_, PushBlobRequestMatcher(), _))
        .WillOnce(Return(grpc::Status::OK));

    pushBlob(uris, qualifiers, digest);
}

TEST_F(ClientTestFixture, PushDirectoryTest)
{
    EXPECT_CALL(*pushClient,
                PushDirectory(_, PushDirectoryRequestMatcher(), _))
        .WillOnce(Return(grpc::Status::OK));

    pushDirectory(uris, qualifiers, digest);
}
